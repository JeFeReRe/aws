const { response, request } = require('express')
const jwt = require('jsonwebtoken')
const Usuario = require("../models/usuario")


const validarJWT = async(req = request, res = response, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(401).json({
            msg: "No hay token de validación"
        });
    }
    try {
        const { uid } = jwt.verify(token, process.env.SECRETORPRIVATEKEY);

        //leer el usuario que corresponde al uid
        const usuario = await Usuario.findById(uid);
        if (!usuario) {
            res.status(400).json({
                msg: "token invalido- usuario no existe en db"
            });
        }

        // Verificar si el uid tiene estado en true
        if (!usuario.estado) {
            return res.status(401).json({
                msg: "usuario eliminado, no puede borrar"
            })
        }
        req.usuario = usuario;

        next();
    } catch (error) {
        console.log(error);
        res.status(400).json({
            msg: "token invalido"
        });
    }
    console.log(token)

}

module.exports = {
    validarJWT
}